#  Star Wars


### Pre requirements
Project use pipenv for virtualenv and packages management
https://github.com/pypa/pipenv#installation


### Installation
In project directory execute following commands
```
pipenv install
./manage.py migrate
./manage.py runserver
```

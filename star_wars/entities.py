from datetime import date
from dataclasses import dataclass


@dataclass
class Person:
    name: str
    height: str
    mass: str
    hair_color: str
    skin_color: str
    eye_color: str
    birth_year: str
    gender: str
    homeworld: str  # resolve url
    date: date  # based on edited

import os

from django.db import models
from django.utils import timezone
from django.conf import settings


def import_path():
    return settings.IMPORT_DIR


class Collection(models.Model):
    created_at = models.DateTimeField(default=timezone.now)
    filepath = models.FilePathField(path=import_path)

    def __str__(self) -> str:
        return os.path.split(self.filepath)[1]

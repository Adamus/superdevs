import requests
from urllib.parse import urljoin
from typing import Generator

from django.conf import settings


def get_all_results(url: str) -> Generator:
    response = requests.get(url)
    data = response.json()
    for record in data["results"]:
        yield record
    url = data["next"]
    if url:
        yield from get_all_results(url)


class SWApi:
    def get_absolute_url(self, relative_url):
        return urljoin(settings.SWAPI_URL, relative_url)

    def people(self) -> Generator:
        url = self.get_absolute_url("/api/people/")
        return get_all_results(url)

    def planets(self) -> Generator:
        url = self.get_absolute_url("/api/planets/")
        return get_all_results(url)

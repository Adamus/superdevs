from star_wars.utils import convert_datetime_string_to_date_str


def test_convert_datetime_string_to_date_str():
    assert (
        convert_datetime_string_to_date_str("2014-12-09T13:50:51.644000Z")
        == "2014-12-09"
    )

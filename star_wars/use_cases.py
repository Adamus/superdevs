import petl as etl

from star_wars.entities import Person
from star_wars.swapi import SWApi
from star_wars.utils import (
    convert_datetime_string_to_date_str,
    get_batch_generator,
    CSVFileContextManager,
)


class ImportPeopleCollectionUseCase:
    def import_and_save_collection(self, batch_size: int, directory: str):
        api = SWApi()
        # Cache planets names to resolve them in next step
        # I assume planets shouldn't take so much memory.
        planet_mapper = {x["url"]: x["name"] for x in api.planets()}

        people_gen = api.people()

        with CSVFileContextManager(directory=directory) as csv_filename:
            write_header = True
            while results := list(get_batch_generator(people_gen, batch_size)):
                tbl = (
                    etl.fromdicts(results)
                    .rename("edited", "date")
                    .convert("date", convert_datetime_string_to_date_str)
                    .convert("homeworld", planet_mapper)
                    .cut(*Person.__annotations__.keys())
                )
                etl.appendcsv(tbl, source=csv_filename, write_header=write_header)
                write_header = False

            return csv_filename


class LoadTableFromCSVUseCase:
    def display_collection(self, filename: str):
        return etl.fromcsv(filename)


class AggregatingColumnsUseCase:
    def aggregate(self, tbl, *columns):
        if len(columns) == 1:
            return tbl.aggregate(columns[0], len).rename("value", "count")
        return tbl.aggregate(columns, len).rename("value", "count")


class ListHeaderUseCase:
    def get_header(self, tbl):
        return tbl.head(0).tuple()[0]


class PaginateUseCase:
    def paginate(self, tbl, page_size: int):
        return tbl.head(page_size)

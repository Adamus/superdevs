import os
import tempfile
from dataclasses import dataclass
from datetime import datetime
from typing import Optional


def convert_datetime_string_to_date_str(datetime_str: str):
    return datetime.strptime(datetime_str, "%Y-%m-%dT%H:%M:%S.%fZ").strftime("%Y-%m-%d")


def get_batch_generator(generator, batch_size: int):
    try:
        while batch_size:
            yield next(generator)
            batch_size -= 1
    except StopIteration:
        pass


@dataclass
class CSVFileContextManager:
    directory: Optional[str]

    def __enter__(self):
        _, self.filename = tempfile.mkstemp(
            prefix="people", suffix=".csv", dir=self.directory
        )
        return self.filename

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type:
            os.remove(self.filename)
            raise
        return False

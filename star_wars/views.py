from dataclasses import dataclass, field
from typing import List

from django.conf import settings
from django.contrib import messages
from django.shortcuts import redirect
from functools import partial
from django.views import generic
from star_wars.models import Collection
from star_wars.use_cases import (
    ImportPeopleCollectionUseCase,
    LoadTableFromCSVUseCase,
    AggregatingColumnsUseCase,
    ListHeaderUseCase,
    PaginateUseCase,
)


partial_split = partial(str.split, sep=",")


@dataclass
class RequestData:
    aggregate: List[str] = field(default_factory=list)
    page_size: int = field(default=10)

    def __post_init__(self):
        if isinstance(self.aggregate, str):
            self.aggregate = partial_split(self.aggregate)
        self.page_size = int(self.page_size)


class CollectionDetailView(generic.DetailView):
    model = Collection

    def get_context_data(self, **kwargs):
        data = super().get_context_data()

        request_data = RequestData(**self.request.GET.dict())

        tbl = LoadTableFromCSVUseCase().display_collection(
            filename=self.object.filepath
        )

        data["columns"] = ListHeaderUseCase().get_header(tbl=tbl)
        data["person_tbl"] = self.get_table(tbl, request_data=request_data)
        data["more"] = request_data.page_size + 10
        return data

    def get_table(self, tbl, request_data):
        if request_data.aggregate:
            return AggregatingColumnsUseCase().aggregate(tbl, *request_data.aggregate)
        return PaginateUseCase().paginate(tbl=tbl, page_size=request_data.page_size)


class CollectionListView(generic.ListView):
    model = Collection


class CollectionImportView(generic.View):
    batch_size = 50  # just to write csv in batches

    def post(self, request, *args, **kwargs):
        try:
            filepath = ImportPeopleCollectionUseCase().import_and_save_collection(
                batch_size=self.batch_size, directory=settings.IMPORT_DIR
            )
        except (ConnectionError, IOError):
            messages.warning(request, "Download failed!")
            return redirect("sw-list")

        collection = Collection.objects.create(filepath=filepath)
        return redirect("sw-detail", pk=collection.pk)

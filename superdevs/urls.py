from django.urls import path

from star_wars.views import (
    CollectionDetailView,
    CollectionListView,
    CollectionImportView,
)

urlpatterns = [
    path("", CollectionListView.as_view(), name="sw-list"),
    path("import/", CollectionImportView.as_view(), name="sw-import"),
    path("<int:pk>/", CollectionDetailView.as_view(), name="sw-detail"),
]
